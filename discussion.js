// CRUD Operations

// 1. INSERT Documents (CREATE)
	/*
	SYNTAX:
		Insert One Document
		db.collectionName.insertOne({
			"fieldA" : "valueA" ,
			"fieldB" : "valueB"
		})

		Insert Many Documents
		db.collectionName.insertMany (
		[
			{	"fieldA" : "valueA" ,
				"fieldB" : "valueB"
			},
			{	"fieldA" : "valueA" ,
				"fieldB" : "valueB"
			},
			
		])
	*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})


db.users.insertMany (
	[
		{	"firstName": "Steve",
			"lastName": "Barnes",
			"age": 76,
			"email": "stevebarnes@mail.com",
			"department": "none"
		} ,

		{	"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "narmstrong@mail.com",
			"department": "none"
		} ,
	])

db.courses.insertMany (
	[
		{	"name": "Javascript 101" ,
			"price": 5000 ,
			"description": "Introduction to Javascript" ,
			"isActive": true
		} ,

		{	"name": "HTML 101" ,
			"price": 2000 ,
			"description": "Introduction to HTML" ,
			"isActive": true
		} ,

		{	"name": "CSS 101" ,
			"price": 2500 ,
			"description": "Introduction to CSS" ,
			"isActive": false
		}
	])

// 2. READ - Find Documents (READ/RETRIEVE)

	/*
		SYNTAX:
			► Retrieve all 
						- this will retrieve all our documents
				
				db.collectionName.find () 

			► Selected 
						- will retrieve all our documents that will match our criteria

				db.collectionName.find ({"criteria" : "value"})
			
			► Find One
						- this will return the first document in our collection

				db.collectionName.findOne ({})

						- this will return the first document that will match our criteria

				db.collectionName.findOne ({"criteria" : "value"})


	*/	


	// retrieve all
	db.users.find ();


	// retrieve selected
	db.users.find (
		{
			"firstName" : "Jane"
		});

	// find one
	db.users.findOne ();

	// find one with criteria
	db.users.findOne (
		{
			"department" : "none"
		});


// 3. UPDATE
	/*
		SYNTAX:

		► Update One
			db.collectionName.updateOne (
				{ 	"criteria" : "value"
				} ,
				{	$set :
					{	"fieldToBeUpdate" : "updatedValue"	
					}	
				}
			)
		► Update Many
			db.collectionName.updateMany (
				{ 	"criteria" : "value"
				} ,
				{	$set :
					{	"fieldToBeUpdate" : "updatedValue"	
					}	
				}
			)
	*/


	db.users.insertOne({
		"firstName": "Test",
		"lastName": "Test",
		"age": 0,
		"email": "test",
		"department": "none"
	})

// update one
db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

//update many
// ► none to HR
	db.users.updateMany (
		{	"department" : "none"	
		},
		{	$set: 
			{	"department" : "HR"
			}
		}
	);

// ► HR to Human Resources
	db.users.updateMany (
		{	"department" : "HR"	
		},
		{	$set: 
			{	"department" : "Human Resources"
			}
		}
	);


// ► removing active status
	db.users.updateOne (
		{ "firstName" : "Bill"
		},
		{	$unset:
			{	"status" : "active"
			}

		}
	);

// ► renaming a field
	db.users.updateMany (
		{},
		{ $rename :
			{
				"department" : "dept"
			}

		}
	);


/*
	MINI ACTIVITY
		1. update HTML 101 
			- isActive to false
		2.  add field to all docs
			- Enrollees : 10 
*/

	// update HTML 101 isActive to false
		db.courses.updateOne (
			{
				"name": "HTML 101" 
			},
			{ 	$set : 
				{
					"isActive": false
				}

			}
		) ;

	// add field to all docs
		db.courses.updateMany (
			{},
			{	$set :
				{
					"enrollees" : 10
				}
			}
		) ;


// 4. DELETE
	/*
		SYNTAX:
			► delete one
			db.collectionName.deleteOne ({"criteria":"value"})	

			► delete many
			db.collectionName.deleteMany ({"criteria":"value"})	 
	*/

db.users.insertOne (
 	{
 		"firstName" : "test"
 	});

// delete test
db.users.deleteOne ({"firstName": "test"});

// delete users in the HR dept
db.users.deleteMany (
	{"dept": "Human Resources"}
	);

// delete all users
du.users.deleteMany ({});